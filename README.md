# Poncilibot

**_Poncili Creacion_ - A surrealist performance group from San Juan, Puerto Rico who specialize in deeply engaging sculptural and spatial manipulation.**

<img src="https://bubbles.leobrown.net/media/de28fd71-12de-4349-af61-d5ea56775b04/32712167_595778220796001_3799425325281050624_n.jpg" wifth="250" alt="Poncili Creacion"/>

## Overview 

Poncilibot is a pretty straight forward script written to better understand the mechanics of posting to a Pleroma instance. Because the API used is identical to the API Mastodon uses for posting, it should be compatible with both. 

Poncilibot's function is to scrape an instagram feed of a a given account, make a list of the posts displayed on that feed, check to see if any of those posts are new, and echo any new posts to an account on a Pleroma instance. 

## Background

I was introduced to federated social media by my friend [@wakest@mastodon.social](https://mastodon.social/@wakest), who is a great resource for thigns decentralized. After describing to them my desire to create a series of bots to auto-curate my online content, they suggested there might be some options within the Federated Social Media, and FOSS community like Mastodon or Pleroma that would be a solid foundation on which to build. 

After a bit of experimentation I decided Pleroma's smaller resource requirements and simplicity was indeed an excellent foundation, and set up a Pleroma instance of my own to house myself and my bots. 

Poncilibot is my initial exploration of working within Pleroma's API to make posts. 

## Contact

Contact me:

[@defnotarobot@bubbles.leobrown.net](https://bubbles.leobrown.net/users/defnotarobot)

Poncilibot Examples:

* [@poncilibot@bubbles.leobrown.net](https://bubbles.leobrown.net/users/poncilibot),
* [@kuniklobot@bubbles.leobrown.net](https://bubbles.leobrown.net/users/kuniklobot),
* [@enormousfacebot@bubbles.leobrown.net](https://bubbles.leobrown.net/users/enormousfacebot),

# Installation

There are certainly modifications that can be made to alter the following requirements, but this is the manner in which Poncilibot was originally designed to run. 

## Install Requirements
* Postgres
* Cron
* Python3
* Pip3
* Virtualenv

## Postgres

Determine which postgres user will be responsible for the bot. In my case, I create a user called 'bots' and a database by the same name that will be responsible for tables related to bot function only. 

This assumes postgres is already installed and user *postgres* has super user priviledges in psql.

```
sudo -u postgres psql
```

Once in the psql prompt:

```
CREATE USER bots;
ALTER USER bots WITH PASSWORD '<password>';
CREATE DATABASE bots;
ALTER DATABASE bots OWNER TO bots;
```

Obviously the password you set for this user will be relevant later. 
Poncilibot will automatically create the tables it requires for operation, so there should be no need to create any tables at this stage. 


## Clone repository

```
git clone https://github.com/voteleobrown/poncilibot
```
## Edit settings.ini

In the `config` folder within the Poncilibot repository, there is a file titled `example_settings.ini`, we will copy this to and modify it. You don't have to use vim, obviously.

```
cd poncilibot
cp config/example_settings.ini
vim config/settings.ini
```

Let's go ahead and plug the username, password, and database name we created in the previous section of the installion into the new `settings.ini` file. You'll be modifying the part that looks like this. No need to use single or double quotes for strings, just type it in as is. 

**Potential Warning:** 
_So yeah, we're storing a postgres password as plaintext here. While true that there's already a line in the .gitignore to prevent you from accidentally committing your settings.ini file to github or something like that, some individals may prefer to modify the code to load login credentials some other way. In my case, I opt to limit the harm that could be done by someone acquiring these credentials -- the 'bots' Postgres user should only be able to modify the 'bots' database, and all of the accounts are relatively unimportant social media bot accounts on a Pleroma instance. However, remember that these plaintext credentials are only as secure as the system on which they are stored._


```
[PSQL]
username: bots
password: <bots_password>
database: <bots_database>
host: localhost
port: 5432
```

This installation assumes you already have an account for the bot you're planning to run. If you're the admin of your Pleroma instance, and you've disabled the registration link on your instance you can run `mix register_user name username email bio password` per instructions given [here](https://git.pleroma.social/pleroma/pleroma/wikis/Admin%20tasks).

Those Pleroma user credentials go in the following section of the `settings.ini` file. 

```
[Account]
base: https://<pleroma_instance>/
username: <pleroma_username>
password: <pleroma_password>
display: <pleroma_display_name>
hashtags: #poncilicreacion
```

The base should be the url of the Pleroma instance and include the first `/` after the domain. 
Username and password are pretty self-explanatory. I do not believe that `display:` overrides the established display name for the user, but there is a value for it later in the API, so I include it as listed for the user created.

The `hashtags:` field is for hashtags you want to go out with every post this bot makes. Presently not a list, so only a single one per bot is functional. 

The following section establishes the username of the Instagram account you're intended to echo. Set `account2follow` to Instagram account you'll be reposting without the '@' symbol. The `follow` value is not currently functional. You can leave it set to true.

```
[Instagram]
follow: true
account2follow: poncilicreacion
```

The following section just designates the folder in which you intend to locally store media between scraping and posting. You can leave the default, unless you have a reason to do otherwise. 

```
[Local]
media_dir: ./media/
```

## Created virtual environment

You'll need python3 and pip3. 

```
sudo apt-get update
sudo apt-get install python3 python3-pip
```
I like to set an alias to make pip3 my default, since I don't python2 outside of a virtual environment. 

```
alias pip='pip3'
```
Keep that in mind in the next step, where if you don't do that, you'll need to explicitly type pip3. 
Then use pip to install virtualenv

```
pip install virtualenv
```

Now initialize the virtual environment. I use a hidden folder named `.venv` for this, and that's how the `.gitignore` file of this repository is prepared.

```
virtualenv -p 'python3' .venv
```

Now enter the repository to install the dependencies. 

```
source .venv/bin/activate
pip install -r requirements.txt
```

And, still in the virtual environment, go ahead and run the script manually the first time before setting it up as a cron task so that you can observe any errors that might occur. 

```
python main.py
```

Assuming there were no errors, the most recent 12 posts from the intended Instagram account should have been reposted by your bot. 

## Cron

You can determine for yourself how often you want to run the script, I generally run it once ever 15 minutes or so, although this feel a little more frequent than is generally necessary with all but the most active accounts. 


`crontab -u <username> -e`

And add this line at the end. 

`*/15 * * * cd /<path>/<to>/<project>/ && /<path>/<to>/<project>/.venv/bin/python /<path>/<to>/<project>/main.py `



# Disection of Functionality

This project is primarily a self-teaching excercise, but it is also my hope that this repository could be a basic resource for anyone else approachign a similar problem. I'm basically about to disect this program as if I were explaining it to a child. For this reason, the following section may seem unnecessarily explicit, and basic for anyone who has any real familiarity with Python, Postgres, RESTful APIs, or even just basic programming in general. 

## main.py (Part I)

This is the core script, and executes all other elements of the application. This is the reason that creating a cron job that runs this script is adequate. 

When the script is run, this conditional runs first:

```
if __name__ == '__main__':
main() 
```

This line determines whether or not the script is running in [main scope](https://docs.python.org/3/library/__main__.html), and if it is, it runs the function `main()`. 

The first thing `main()` does is create an instance of class `Config`, which it loaded from config.py at the top of the script with the line:

```from config import Config```

That class is assigned to the variable `conf`.

## conf.py

conf.py contains the class `Config`, which loads seetings stored in `./config/settings.ini` into variables to be used during the execution of main.py.

This repository contains a file `./config/settings_example.ini` which can be copied to `./config/settings.ini` and altered to include the propers settings. More information regarding the `settings.ini` file can be found in the ['Edit settings.ini'](##Edit-settings.ini) section of this document. 

conf.py uses the library [configparser](https://docs.python.org/3/library/configparser.html) to load the `settings.ini` using the following lines:

```
self.Config = configparser.ConfigParser()
self.Config.read('config/settings.ini')
```

Then the function `ConfigSectionMap(config_section)` within the class allows dictionary access of the various sections of the `settings.ini` file. 

For example:

```
self.username = self.ConfigSectionMap('Account')['username']
```

Thus after the `Config` class is assigned to the variable `conf`, that variable contains subscriptable values for each line of the `settings.ini` at the following locations:

| location              |     value                                                |
|---------------------- | -------------------------------------------------------- |
| conf.username         | Username of the pleroma account corresponding to the bot |
| conf.password         | Password of the pleroma account corresponding to the bot |
| conf.displayName      | Display name of the pleroma account corresponding to the bot | 
| conf.baseURL          | Base url for the desired pleroma instance                |
| conf.postHashtags     | Presently only supports one hashtag at a time            |
| conf.instagram2follow | The Instagram account the bot is intended to follow      | 
| conf.psqlUsername     | Username for logging into postgres                       |
| conf.psqlPassword     | Password for logging into postgres                       |
| conf.psqlDatabase     | Postgres database to be used by bot                      |
| conf.psqlHost         | Postgres host (ex. 'localhost')                          |
| conf.psqlPort         | Postgres port (ex. 5432)                                 |
| conf.psqlTable        | Presently conf.username + '_instagram'                   |
| conf.localmedia       | Directory in which downloaded media will be stored.      |


## main.py (Part II)

Next `main()` uses the Config class assigned to `conf` to create an instance of `psqlClass` with the line:

```
curs = psqlClass(conf)
```

This class, `psqlClass`, was imported from psql.py with the line 

```
from psql import psqlClass
```

This class is used for handling interactions with the postgres database for the bot. Postgres is used to store which posts have already been made and thus prevent duplicate postings. There are a number of functions that are either called locally when initializing the class, or later during various processes. 

## psql.py

psql.py is built around the library [psycopg2](http://initd.org/psycopg/docs/). 

### Functions in psqlClass

#### psqlConnect()

Creates the postgres cursor used to interact with database using the credentials from the passed Config class and assigns it to the variable `self.cursor`. This variable is then used by every other function in the class. 

#### tableExists()

Determines whether or not a given table exists, in this case the table used for storing bot data. Returns true if table does exist. 

#### entryExists()

Like above but more generic (any column and value in a given table).

#### uploadEntered()

Determine whether a given Instagram post already has a media upload id return from Mastodon API. Checks for presence of a value in the `upload_id` column of the bot's instagram table. 

#### getUploadId()

Returns media id for given Instagram post (requires the unique id found in Instagram feed for that post).

#### statusCreated()

Determine if a status has already been generated for a given Instagram post by referencing the `status_created` column in the bot's istagram table. 

#### insertNewPost()

Inserts row in instagram table with values in columns `post_id`, `media_url`, `is_video`, `post_text`. Executed when a post is discovered (before posting to pleroma)

#### updateAfterPost()

Inserts additional values in columns of appropriate row after a post has been made to pleroma. 


## instagram.py

This script does the majority of the work of scraping the intended user's Instagram feed (using the class instaScrape) and making the new post on the pleroma instance (via instaPost). 

Both of these classes were imported into `main.py` with the line

```python
from instagram import instaScrape, instaPost
```

The first of them to be used is instaScrape, which is assigned to the variable `Instagram` with the line

```python
Instagram = instaScrape(conf.instagram2follow)
```

When instagram.py was imported, a few other dependencies were imported along with it. The class instaScrape uses Beautiful Soup (bs4) and requests to fetch and parse the Instagram profile of the intended account. Let's take a closer look at how this works.

First lets look over the entire class:

```python
class instaScrape:
    
    '''
    Scrape Instagram feed for recent posts
    '''
    
    def __init__(self, account):
        self.baseURL = 'https://instagram.com/'
        self.r = requests.get(self.baseURL + account + '/')
        self.soup = BeautifulSoup(self.r.text, "lxml")
        self.errors = []

        self.scripts = self.soup.find_all('script')
        for script in self.scripts:
            if "window._sharedData" in script.text:
                try:
                    self.j = json.loads(script.text.split(" = ")[1].strip(";"))         

                    self.edges = self.j['entry_data']['ProfilePage'][0]['graphql']['user']['edge_owner_to_timeline_media']['edges']
                except Exception as e:
                    try:
                        self.errors.append(e)
                        self.errors.append(sys.exc_info()[0])
                    except:
                        pass
             
        self.posts = []
            

        for edge in self.edges:
            edgeJSON = { 'media_url': edge['node']['display_url'],
                'post_text': edge['node']['edge_media_to_caption']['edges'][0]['node']['text'], 
                'id': edge['node']['id'],
                'is_video': edge['node']['is_video']}

self.posts.append(edgeJSON)
```

I now that much of this README is incredibly granular, but I'm not entirely prepared to do a complete dissection of classes and object oriented programming in Python. If the follow section is confusing, good references include:

* [Classes (Python3 documentation)](https://docs.python.org/3/tutorial/classes.html)
* [Object Oriented Programming](https://python.swaroopch.com/oop.html)
* [Classes](http://introtopython.org/classes.html)

The first thing instaScrape does with the `__init__()` function, which initializes the class, is establish some local variables that will help in fetching the contents of the desired Instagram profile. Notice that `__init__()` expects a variable it calls `account` to be passed to the class when initializing. If we look back at how the class was initialized in main.py, we'll see the variable `conf.instagram2follow` is passed to instaScrape when initializing it and assigning it to the variable Instagram. 

We could then go back to config.py to establish that `conf.instagram2follow` is derived from the Instagram account name we configured in settings.ini 

In our example case, let's say that `conf.instagram2follow = poncilicreacion`

Back to the `__init__()`, let's focus on the following lines:

```python
        self.baseURL = 'https://instagram.com/'
        self.r = requests.get(self.baseURL + account + '/')
        self.soup = BeautifulSoup(self.r.text, "lxml")
```

First, the function `__init__()` establishes a local variable callsed `self.baseURL`. In the next line, it combines that variable with `account` and `'/'` to make a string that will be the URL of the desired instagram profile. In our example case that would be `'https://instagram.com/poncilicreacion/'`. 

Remember that instagram.py imports the [requests](http://docs.python-requests.org/en/master/) library. The function `requests.get()` then fetches the contents of the url it is passed as input, in this case `requests.get('https://instagram.com/poncilicreacion/')` fetches the contents of the Poncili Creacion Instagram profile and assigns what it retrieves to the variable `self.r`.

[Beautiful Soup](https://www.crummy.com/software/BeautifulSoup/bs4/doc/) is a library that extracts elements from html and organizes them for other uses. In this case we pass `BeautifulSoup()` the plaintext contents of the page we retrieved using requests, and identify the desired HTML parser (lxml) and assign the results to the local variable `self.soup`.


Once we've made our soup, we need to isolate the desired HTML. This is the portion of the class responsible for that: 

```python
        self.scripts = self.soup.find_all('script')
        for script in self.scripts:
            if "window._sharedData" in script.text:
                try:
                    self.j = json.loads(script.text.split(" = ")[1].strip(";"))         

                    self.edges = self.j['entry_data']['ProfilePage'][0]['graphql']['user']['edge_owner_to_timeline_media']['edges']
                except Exception as e:
                    try:
                        self.errors.append(e)
                        self.errors.append(sys.exc_info()[0])
                    except:
                        pass
             
        self.posts = []
            

        for edge in self.edges:
            edgeJSON = { 'media_url': edge['node']['display_url'],
                'post_text': edge['node']['edge_media_to_caption']['edges'][0]['node']['text'], 
                'id': edge['node']['id'],
                'is_video': edge['node']['is_video']}

        self.posts.append(edgeJSON)
```

We'll ignore the error handling for now. 

Instagram apparently uses GraphQL. Lack of knowledge about GraphQL isn't of much consequence, but does help explain the way things are constructed on the page we've retreived. We're looking for some JSON that Instagram delivers inside of `<script> </script>` tags. This JSON contains all the information about the posts displayed on the profile. This was determined, of course, by reading through the page source. We'll need our script to find the appropriate tags, and then clean them up so we can use the information stored in that JSON. 

We've already established that `self.soup` is our parsed and organized HTML made by BeautifulSoup. The function [find_all()](https://www.crummy.com/software/BeautifulSoup/bs4/doc/#find-all) within the `self.soup` object allows its contents to be searched using defined [filters](https://www.crummy.com/software/BeautifulSoup/bs4/doc/#kinds-of-filters). In this case we'll find all `<script>` tags and assign them to the `self.scripts` variable using the line:

```python
        self.scripts = self.soup.find_all('script')
```

This creates a list of all instances of the `<script>` that we can iterate through. We'll iterate through that list looking for one that contains `window._sharedData`. Once we've found that tag, we'll clean it into useable JSON, convert that into a [Python dictionary](https://docs.python.org/3/tutorial/datastructures.html#dictionaries)  and assign it to the variable `self.j`  

```python
self.j = json.loads(script.text.split(" = ")[1].strip(";")) 
```

Within `self.j` there is an element called 'edges' which contains all of the Instagram post information, but because the JSON heirarchy used to store them is a bit convoluted and contains unnecessary information, we'll extract this element, and then create a new list that's a bit more concise

```python 
self.edges = self.j['entry_data']['ProfilePage'][0]['graphql']['user']['edge_owner_to_timeline_media']['edges']
```
Then:

```python
        self.posts = []
            

        for edge in self.edges:
            edgeJSON = { 'media_url': edge['node']['display_url'],
                'post_text': edge['node']['edge_media_to_caption']['edges'][0]['node']['text'], 
                'id': edge['node']['id'],
                'is_video': edge['node']['is_video']}

        self.posts.append(edgeJSON)
```
At this point, every entry in the list `self.posts` represents a distinct Instagram post, and contains the following elements:

| key          | description                                 |
|--------------|---------------------------------------------|
| media_url    | The url of the media contained within the post | 
| post_text    | The text associated with the instagram post |
| id           | A unique identifier assigned to the post by Instagram |
| is_video     | A boolean value stating whether or not the post is a video | 

Let's assess what we have here. The page we retrieved using requests should have contained information related to the 12 most recent posts on that profile. We have parsed that page and extracted the info regarding those twelve posts. Because the purpose of poncilibot is to echo the content of a desired Instagram user's profile, it will need to make posts of its own on a pleroma client. Additionally, if the main.py script on poncilibot is run every 15 minutes, in all likelihood most of the times it exectutes, no new posts will have been created, and the 12 posts it retrieves will be posts it has already processed before, so we will need to create some sort of record, so that the same post is not repeated twice. 

## processPost()

Each post in the `Instagram.posts` list needs to be processed individually. We'll do this using the `processPost()` function which is defined in main.py. 

```python
    for post in reversed(Instagram.posts):
        processPost(curs,conf,post)
```

Notice that when iterating through `Instagram.posts` we first reverse its order using `reversed()`. This has to do with the order in which posts should appear. 

`processPost()` requires that it be passed a cusor object, a config object, and a post object from our list of posts. Since the construction of these has already been outlined, that should make sense, so lets take a look at what happens once `processPost` is executed. 

```python
def processPost(cursorClass,configClass,post):
    '''
    Used to process a given post when iterating through scraping results.
    '''
    uploaded = False
    print("Checking if post is already in '" + configClass.psqlTable + "'...")
    if cursorClass.entryExists('post_id',post['id']) == True:
        print("Entry for " + post['id'] + " already exists.")
    else:
        try:
            cursorClass.insertNewPost(post['id'],post['media_url'],post['is_video'],post['post_text'])
            print("Post " + post['id'] + " added successfully:")
            print(post)
        except Exception as e:
            print(e)

    print("Checking if media has already been uploaded...")
    if cursorClass.uploadEntered(post['id']) == True:
        uploaded = True
        upload_id = cursorClass.getUploadId(post['id'])
        print('Media already uploaded: ' + post['id'])
        
    print("Checking if status already posted...")
    if cursorClass.statusCreated(post['id']) == True:
        print("Status already generated for Instagram post " + post['id'] + ".")
    else:
        print("Instagram post " + post['id'] + " is new.")            

        print("Checking that local copy of media exists...")
        if mediaExistsLocal(mediaURL2Local(post['media_url'],configClass.localMedia)) == True:
            print("Local copy of media does already exist.")
        else:
            print("Local copy of media does not exist, downloading now...")
            try:
                downloadImage(post['media_url'],configClass.localMedia)
                print("Download complete, media saved to " + mediaURL2Local(post['media_url'],configClass.localMedia))
            except Exception as e:
                print(e)
    
        print("Posting status...")
        newStatus = instaPost(configClass,post)
    
        print("Updating database...")
        try:
            cursorClass.updateAfterPost(post['id'],newStatus)
            print("Success.")
        except Exception as e:
            print(e)
        
        
        if uploaded == True:
print("Previous upload_id existed, but post had not previously been made. Weird, but database was updated regardless.")
```
This function runs once for each post to be processed. 

### Determine if the post has been seen before

First, it checks to see if the unique identifier given the post by Instagram is already listed in a row of the postgres table that was created by the bot. If the id is not already listed, the bot creates a new entry for it. 

```python
    print("Checking if post is already in '" + configClass.psqlTable + "'...")
    if cursorClass.entryExists('post_id',post['id']) == True:
        print("Entry for " + post['id'] + " already exists.")
    else:
        try:
            cursorClass.insertNewPost(post['id'],post['media_url'],post['is_video'],post['post_text'])
            print("Post " + post['id'] + " added successfully:")
            print(post)
        except Exception as e:
            print(e)
```

This requires `insertNewPost()` from psql.py, which is passed to `processPost()` in cursorClass (as it's called locally, which practically speaking is the `curs` object. 

Just as reminder that function works like so:

```python
    def insertNewPost(self,insta_id,media_url,is_video,post_text):
        query = "INSERT INTO %s (post_id, media_url, is_video, post_text) VALUES ('%s','%s',%s,'%s')"
        self.cursor.execute(query % (self.table,insta_id,media_url,is_video,escapes(str(post_text))))
```

Which basically just puts the values we collected from the post in the last step into the appropriate column of a new row in the table. 

### Determining if the media content for the post has been acquired

At the beginning of `processPost`, a variable called uploaded was set to be False

```python
 uploaded = False
```
Using the `uploadEntered()` function within the postgres cursor class, the next step in `processPost()` is to determine if the media from the scraped post has already been uploaded. 

```python
    print("Checking if media has already been uploaded...")
    if cursorClass.uploadEntered(post['id']) == True:
        uploaded = True
        upload_id = cursorClass.getUploadId(post['id'])
        print('Media already uploaded: ' + post['id'])
```

Here, if `uploadEntered()` queries the table to determine if an upload id has already been entered for the row with the corresponding post id. If there is a result, then the variable upload is set to True, and the upload id for that post id is stored in the variable `upload_id`. 

Next `processPost()` checks to determine if the status has already been marked as posted. If it has, then the bot can skip it. If it has not already been posted, the the bot goes through the procedure of making the post. First it 

```python
   print("Checking if status already posted...")
    if cursorClass.statusCreated(post['id']) == True:
        print("Status already generated for Instagram post " + post['id'] + ".")
``` 

If it has, then the bot can skip it. If it has not already been posted, the the bot goes through the procedure of making the post.  

``` python
    else:
        print("Instagram post " + post['id'] + " is new.")            ``` 
``` 

First it checks the media folder of the system it's running on to determine if a local copy of the media has been saved previously. This is done with the function `mediaExistsLocal()` from tools.py.

````python
def mediaExistsLocal(filename):
    '''
    Check to see if the file has already been downloaded
    '''
    file = Path(filename)
    if file.is_file():
        return True
    else:
        return False
````

If it has not been previously downloaded, then it is downloaded using `downloadImage()`, also from tools.py.

````python
def downloadImage(url,localDir):
    '''
    Download media to local directory
    '''
    filename = mediaURL2Local(url,localDir)
    response = requests.get(url)
    if response.status_code == 200:
        with open(filename, 'wb') as f:
            f.write(response.content)
````

This portion of main.py handles that:

````python
        print("Checking that local copy of media exists...")
        if mediaExistsLocal(mediaURL2Local(post['media_url'],configClass.localMedia)) == True:
            print("Local copy of media does already exist.")
        else:
            print("Local copy of media does not exist, downloading now...")
            try:
                downloadImage(post['media_url'],configClass.localMedia)
                print("Download complete, media saved to " + mediaURL2Local(post['media_url'],configClass.localMedia))
            except Exception as e:
                print(e)
````

### Making the Post


Once there is a local copy of the media, the bot has all it needs to make the post. Posting is accomplished using the `instaPost` class from instagram.py. The class is used by passing it a config object, and assigning it to an object like so:

````python
        print("Posting status...")
        newStatus = instaPost(configClass,post)
````

#### instaPost

````python
class instaPost:
    
    '''
    Make post using content from scraped Instagram feed using Mastodon API
    '''
    
    def __init__ (self,configObject,post):      
        self.username = configObject.username
        self.password = configObject.password
        self.displayname = configObject.displayName
        self.instagram2follow = configObject.instagram2follow
        self.filename = isolateFilename(post['media_url'])
        self.post_text = post['post_text']
        self.is_video = post['is_video']

        self.instaAccount = configObject.instagram2follow

        self.postHashtags = configObject.postHashtags
        self.uploadURL = configObject.baseURL + 'api/v1/media'
        self.postURL = configObject.baseURL + 'api/statuses/update.json'
        self.media_dir = configObject.localMedia
        
        self.upload_resp = 0
        self.status_resp = 0
        
        try:
            self.uploadFile()
        except Exception as e:
            print(e)
        if self.upload_resp == 200:
            try:
                self.postStatus()
                if self.status_resp == 200:
                    print('Post made.')
                else:
                    print('Post not made. Post response code: ' + str(self.post_resp))
            except Exception as e:
                print(e)
        else: 
            print('Upload not successful. Upload repsonse code: ' + str(self.upload_resp))

    def uploadFile(self):
        '''
        Upload media attachment using Mastodon API
        '''
        self.file_dir = self.media_dir + self.filename
        self.file = open(self.file_dir,'rb')
        self.files_data = { 'file': self.file }

        try:
            resp = requests.post(self.uploadURL,files=self.files_data, auth=(self.username,self.password))
            self.upload_resp = resp.status_code
            self.upload_body = resp.json()
        except Exception as e:
            print(e)
            self.upload_resp = e
    
    def postStatus(self):
        '''
        Post status with media attachment using Mastodon API
        '''
        self.post_data = { 'status': self.post_text + '\n\n[From https://instagram.com/' + self.instagram2follow + ' ]\n\n' + self.postHashtags, 
                          'source': self.displayname, 
                          'media_ids': self.upload_body['id'] }
        try:
            resp = requests.post(self.postURL, data=self.post_data, auth=(self.username,self.password))
            self.status_resp = resp.status_code
            self.status_body = resp.json()
        except Exception as e:
            print(e)
            self.status_rep = e
````






````python
        print("Updating database...")
        try:
            cursorClass.updateAfterPost(post['id'],newStatus)
            print("Success.")
        except Exception as e:
            print(e)
````




## Uploading Media

post.upload_body:

````
{ id: '97575',
 preview_url: 'https://<pleroma_instance>/media/<directory>/<filename>.jpg',
 remote_url: 'https://<pleroma_instance>/media/<directory>/<filename>.jpg',
 text_url: 'https://<pleroma_instance>/media/<directory>/<filename>.jpg',
 type: 'image',
 url: 'https://<pleroma_instance>/media/<directory>/<filename>.jpg'}
````

post.status_body:


```
{ activity_type: 'post',
 attachments: [{ id: 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx',
                  mimetype: 'image/jpeg',
                  oembed: False,
                  url: 'https://<pleroma_instance>/media/<directory>/<filename>.jpg'}],
 attentions: [],
 created_at: 'Wed May 16 00:50:10 +0000 2018',
 external_url: 'https://<pleroma_instance>/objects/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx',
 fave_num: 0,
 favorited: False,
 id: xxxxx,
 in_reply_to_status_id: None,
 is_local: True,
 is_post_verb: True,
 possibly_sensitive: False,
 repeat_num: 0,
 repeated: False,
 statusnet_conversation_id: xxxxx,
 statusnet_html: '🔥Post including <html>',
 tags: [ 'tag_array_item'],
 text: '🔥Post text as it is displayed',
 uri: 'https://<pleroma_instance>/objects/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx',
 user: { background_image: None,
          cover_photo: 'https://<pleroma_instance>/media/<directory>/<filename>.jpeg',
          created_at: 'Sat May 05 04:37:49 +0000 2018',
          description: 'Bio of account making the post.',
          favourites_count: 0,
          followers_count: 0,
          following: True,
          follows_you: True,
          friends_count: 0,
          id: 592,
          is_local: True,
          name: 'Displayname of account making the post.',
          profile_image_url: 'https://<pleroma_instance>/media/<directory>/<profile_picture_filename>.jpeg',
          profile_image_url_https: 'https://<pleroma_instance>/media/<directory>/<profile_picture_filename>.jpeg',
          profile_image_url_original: 'https://<pleroma_instance>/media/<directory>/<profile_picture_filename>.jpeg',
          profile_image_url_profile_size: 'https://<pleroma_instance>/media/<directory>/<profile_picture_filename>.jpeg',
          rights: {'delete_others_notice': False},
          screen_name: '<pleroma_user>',
          statuses_count: 1,
          statusnet_blocking: False,
          statusnet_profile_url: 'https://<pleroma_instance>/users/<pleroma_user>'}}
```

# TODO

* Multi-image Instagram posts are not presently being handled.
* Video posts are not presently being handled as videos, and just post preview image.
* Adding Twitter functionality. 

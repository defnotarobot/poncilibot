import configparser

class Config:
    '''
    Load settings.ini
    '''
    def __init__(self):
        self.Config = configparser.ConfigParser()
        self.Config.read('config/settings.ini')
        self.username = self.ConfigSectionMap('Account')['username']
        self.password = self.ConfigSectionMap('Account')['password']
        self.displayName = self.ConfigSectionMap('Account')['display']
        self.baseURL = self.ConfigSectionMap('Account')['base']
        
        self.postHashtags = self.ConfigSectionMap('Account')['hashtags']

        self.instagram2follow = self.ConfigSectionMap('Instagram')['account2follow']

        self.psqlUsername = self.ConfigSectionMap('PSQL')['username']
        self.psqlPassword = self.ConfigSectionMap('PSQL')['password']
        self.psqlDatabase = self.ConfigSectionMap('PSQL')['database']
        self.psqlHost = self.ConfigSectionMap('PSQL')['host']
        self.psqlPort = self.ConfigSectionMap('PSQL')['port']
        self.psqlTable = self.username + '_instagram'
      
        self.localMedia = self.ConfigSectionMap("Local")['media_dir']

    def ConfigSectionMap(self,section):
        dict1 = {}
        options = self.Config.options(section)
        for option in options:
            try:
                dict1[option] = self.Config.get(section, option)
                if dict1[option] == -1:
                    DebugPrint("skip: %s" % option)
            except:
                print("exception on %s!" % option)
                dict1[option] = None
        return dict1

if __name__ == '__main__':
    print("This intended to exist as dependency only.")

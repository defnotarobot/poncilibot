from bs4 import BeautifulSoup
from tools import *
import requests
import json


class instaScrape:
    
    '''
    Scrape Instagram feed for recent posts
    '''
    
    def __init__(self, account):
        self.baseURL = 'https://instagram.com/'
        self.r = requests.get(self.baseURL + account + '/')
        self.soup = BeautifulSoup(self.r.text, "lxml")
        self.errors = []

        self.scripts = self.soup.find_all('script')
        for script in self.scripts:
            if "window._sharedData" in script.text:
                try:
                    self.j = json.loads(script.text.split(" = ")[1].strip(";"))         

                    self.edges = self.j['entry_data']['ProfilePage'][0]['graphql']['user']['edge_owner_to_timeline_media']['edges']
                except Exception as e:
                    try:
                        self.errors.append(e)
                        self.errors.append(sys.exc_info()[0])
                    except:
                        pass
             
        self.posts = []
            

        for edge in self.edges:
            edgeJSON = { 'media_url': edge['node']['display_url'],
                'post_text': edge['node']['edge_media_to_caption']['edges'][0]['node']['text'], 
                'id': edge['node']['id'],
                'is_video': edge['node']['is_video']}

            self.posts.append(edgeJSON)


class instaPost:
    
    '''
    Make post using content from scraped Instagram feed using Mastodon API
    '''
    
    def __init__ (self,configObject,post):      
        self.username = configObject.username
        self.password = configObject.password
        self.displayname = configObject.displayName
        self.instagram2follow = configObject.instagram2follow
        self.filename = isolateFilename(post['media_url'])
        self.post_text = post['post_text']
        self.is_video = post['is_video']

        self.instaAccount = configObject.instagram2follow

        self.postHashtags = configObject.postHashtags
        self.uploadURL = configObject.baseURL + 'api/v1/media'
        self.postURL = configObject.baseURL + 'api/statuses/update.json'
        self.media_dir = configObject.localMedia
        
        self.upload_resp = 0
        self.status_resp = 0
        
        try:
            self.uploadFile()
        except Exception as e:
            print(e)
        if self.upload_resp == 200:
            try:
                self.postStatus()
                if self.status_resp == 200:
                    print('Post made.')
                else:
                    print('Post not made. Post response code: ' + str(self.post_resp))
            except Exception as e:
                print(e)
        else: 
            print('Upload not successful. Upload repsonse code: ' + str(self.upload_resp))

    def uploadFile(self):
        '''
        Upload media attachment using Mastodon API
        '''
        self.file_dir = self.media_dir + self.filename
        self.file = open(self.file_dir,'rb')
        self.files_data = { 'file': self.file }

        try:
            resp = requests.post(self.uploadURL,files=self.files_data, auth=(self.username,self.password))
            self.upload_resp = resp.status_code
            self.upload_body = resp.json()
        except Exception as e:
            print(e)
            self.upload_resp = e
    
    def postStatus(self):
        '''
        Post status with media attachment using Mastodon API
        '''
        self.post_data = { 'status': self.post_text + '\n\n[From https://instagram.com/' + self.instagram2follow + ' ]\n\n' + self.postHashtags, 
                          'source': self.displayname, 
                          'media_ids': self.upload_body['id'] }
        try:
            resp = requests.post(self.postURL, data=self.post_data, auth=(self.username,self.password))
            self.status_resp = resp.status_code
            self.status_body = resp.json()
        except Exception as e:
            print(e)
            self.status_rep = e

if __name__ == '__main__':
    print("This script intended to exist as dependency only.")

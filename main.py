import time
from config import Config
from psql import psqlClass
from instagram import instaScrape, instaPost
from tools import *
import requests

def processPost(cursorClass,configClass,post):
    '''
    Used to process a given post when iterating through scraping results.
    '''
    uploaded = False
    print("Checking if post is already in '" + configClass.psqlTable + "'...")
    if cursorClass.entryExists('post_id',post['id']) == True:
        print("Entry for " + post['id'] + " already exists.")
    else:
        try:
            cursorClass.insertNewPost(post['id'],post['media_url'],post['is_video'],post['post_text'])
            print("Post " + post['id'] + " added successfully:")
            print(post)
        except Exception as e:
            print(e)

    print("Checking if media has already been uploaded...")
    if cursorClass.uploadEntered(post['id']) == True:
        uploaded = True
        upload_id = cursorClass.getUploadId(post['id'])
        print('Media already uploaded: ' + post['id'])
        
    print("Checking if status already posted...")
    if cursorClass.statusCreated(post['id']) == True:
        print("Status already generated for Instagram post " + post['id'] + ".")
    else:
        print("Instagram post " + post['id'] + " is new.")            

        print("Checking that local copy of media exists...")
        if mediaExistsLocal(mediaURL2Local(post['media_url'],configClass.localMedia)) == True:
            print("Local copy of media does already exist.")
        else:
            print("Local copy of media does not exist, downloading now...")
            try:
                downloadImage(post['media_url'],configClass.localMedia)
                print("Download complete, media saved to " + mediaURL2Local(post['media_url'],configClass.localMedia))
            except Exception as e:
                print(e)
    
        print("Posting status...")
        newStatus = instaPost(configClass,post)
    
        print("Updating database...")
        try:
            cursorClass.updateAfterPost(post['id'],newStatus)
            print("Success.")
        except Exception as e:
            print(e)
        
        
        if uploaded == True:
            print("Previous upload_id existed, but post had not previously been made. Weird, but database was updated regardless.")


        

def main():
    '''
    Scrape instagram feed of desired user, check postgres database to determine if posts have already been 
    echoed to Pleroma, and create statuses new posts.
    '''
    # Load './config/settings.ini'
    conf = Config()

    # Connect to postgres
    curs = psqlClass(conf)

    # Scrape the instagram feed of the desired account
    print("Scraping Instagram feed for " + conf.instagram2follow + "...")
    Instagram = instaScrape(conf.instagram2follow)
    print("Done.")

    # Iterate through results of scrape, and process the discovered posts.
    for post in reversed(Instagram.posts):
        processPost(curs,conf,post)
        
    print("Instagram echo completed.")


if __name__ == '__main__':
    main()   

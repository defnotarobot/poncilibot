import psycopg2
from tools import asc2timestamp

def escapes(string):
    string = string.replace("'", "''")
    string = string.replace('"', '""')
    return string

class psqlClass:
    def __init__(self, configObject):
        self.username = configObject.psqlUsername
        self.password = configObject.psqlPassword
        self.database = configObject.psqlDatabase
        self.host = configObject.psqlHost
        self.port = configObject.psqlPort
        self.table = configObject.psqlTable
        
        self.cursor = self.psqlConnect()
        
        # Check for existence of '<username>_instagram' table, and create it if absent
        print("Checking for Instagram post id in table '" + self.table + "'...")
        if self.tableExists() == True:
            print('Table alreay exists.')
        else:
            print("Creating table '" + self.table + "'...")
            self.ctQuery = ("CREATE TABLE %s ("
                     "row_id BIGSERIAL PRIMARY KEY, "
                     "entry_time TIMESTAMP DEFAULT now(), "
                     "post_id TEXT, media_url TEXT, "
                     "is_video BOOLEAN, post_text TEXT, "
                     "upload_id TEXT, "
                     "upload_url TEXT, "
                     "status_text TEXT, "
                     "status_id TEXT, "
                     "status_conversation TEXT, "
                     "status_created BOOLEAN DEFAULT false, "
                     "status_url TEXT, "
                     "status_time TIMESTAMP)")
            self.cursor.execute(self.ctQuery % (self.table))
            print("Done.")

    def psqlConnect(self):
        '''
        Connect to postgres and return cursor
        '''
        con = psycopg2.connect("dbname=%s user=%s host=%s password=%s"% (self.database,self.username,self.host,self.password))
        con.autocommit = True
        cur = con.cursor()
        return cur
    
    def tableExists(self):
        '''
        Check if table exists
        '''
        self.cursor.execute("SELECT EXISTS(SELECT * FROM information_schema.tables WHERE table_name='%s')" % (self.table))
        result = self.cursor.fetchone()[0]
        return result
    
    def entryExists(self,column,value):
        '''
        Check if row exists using specified column and value
        '''
        self.cursor.execute("SELECT EXISTS(SELECT * FROM %s WHERE %s='%s')" % (self.table,column,value))
        result = self.cursor.fetchone()[0]
        return result
    
    def uploadEntered(self, insta_id):
        '''
        Determine whether a given Instagram post already has a media upload id return from Mastodon API
        '''
        self.cursor.execute("SELECT EXISTS(SELECT upload_id FROM %s WHERE post_id='%s')" % (self.table,insta_id))
        result = self.cursor.fetchone()[0]
        return result
    
    def getUploadId(self, insta_id):
        '''
        Return media id for given Instagram post
        '''
        self.cursor.execute("SELECT upload_id FROM %s WHERE post_id='%s'" % (self.table,insta_id))
        result = self.cursor.fetchone()[0]
        return result
    
    def statusCreated(self,insta_id):
        '''
        Determine if a status has already been generated for a given Instagram post
        '''
        self.cursor.execute("SELECT status_created FROM %s WHERE post_id='%s'" % (self.table,insta_id))
        result = self.cursor.fetchone()[0]
        return result
    
    def insertNewPost(self,insta_id,media_url,is_video,post_text):
        query = "INSERT INTO %s (post_id, media_url, is_video, post_text) VALUES ('%s','%s',%s,'%s')"
        self.cursor.execute(query % (self.table,insta_id,media_url,is_video,escapes(str(post_text))))
        
    def updateAfterPost(self,insta_id,newPost):
        query = ("UPDATE %s SET upload_id='%s', "
            "upload_url='%s', "
            "status_text='%s', "
            "status_id='%s', "
            "status_conversation='%s', "
            "status_url='%s', "
            "status_created=true, "
            "status_time='%s'::TIMESTAMP WHERE post_id='%s'")
        self.cursor.execute(query % (self.table,str(newPost.upload_body['id']),newPost.upload_body['url'],escapes(str(newPost.status_body['text'])),str(newPost.status_body['id']),str(newPost.status_body['statusnet_conversation_id']),newPost.status_body['external_url'],asc2timestamp(newPost.status_body['created_at']).strftime('%Y-%m-%dT%H:%M:%SZ'),insta_id))        

if __name__ == '__main__':
    print("This script intended to exist as dependency only.") 

from pathlib import Path
import requests
import datetime

#########################
# Local File Management #
#########################

def mediaURL2Local(url,localDir):
    '''
    Take Instagram image url and turn it into a path for local download
    '''
    directory = localDir
    filename = url.split("/")
    filename = directory + filename[(len(filename)-1)]
    return filename

def isolateFilename(url):
    '''
    Take Instagram image url and return just the filename
    '''
    filename = url.split("/")
    filename = filename[(len(filename)-1)]
    return filename

def mediaExistsLocal(filename):
    '''
    Check to see if the file has already been downloaded
    '''
    file = Path(filename)
    if file.is_file():
        return True
    else:
        return False

def downloadImage(url,localDir):
    '''
    Download media to local directory
    '''
    filename = mediaURL2Local(url,localDir)
    response = requests.get(url)
    if response.status_code == 200:
        with open(filename, 'wb') as f:
            f.write(response.content)

#########
# Misc. #
#########

def asc2timestamp(ascString):
    '''
    Convert the Mastodon format created_at timestamp to python time object
    '''
    return datetime.datetime.strptime(ascString,'%a %b %d %H:%M:%S %z %Y')
